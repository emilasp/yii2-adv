<?php
namespace emilasp\adv;

use yii\helpers\ArrayHelper;
use emilasp\core\CoreModule;
use emilasp\settings\models\Setting;
use emilasp\settings\behaviors\SettingsBehavior;

/**
 * Class AdvModule
 * @package emilasp\adv
 */
class AdvModule extends CoreModule
{
    public function behaviors()
    {
        return ArrayHelper::merge([
            /*'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => 'Справочники',
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'    => 'variety_cache_duration',
                        'name'    => 'Кеширвание справочников',
                        'description' => 'Время кеширования справочников',
                        'default' => 300,
                    ],
                ],
            ],*/
        ], parent::behaviors());
    }
}
