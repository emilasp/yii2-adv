<?php

namespace emilasp\adv\controllers;

use emilasp\adv\models\forms\DirectForm;
use emilasp\adv\models\forms\KeyXForm;
use yii;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use emilasp\core\components\base\Controller;

/**
 * KeyServices implements the CRUD actions for Seo model.
 */
class KeyServicesController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index'],
                'rules'        => [
                    [
                        'actions' => ['index'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Seo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model  = new DirectForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->generate();
        }
        
        return $this->render('index', [
            'model'  => $model,
        ]);
    }

    /**
     * Lists all Seo models.
     * @return mixed
     */
    public function actionKeyX()
    {
        $model  = new KeyXForm();

        if ($model->load(Yii::$app->request->post())) {
            $model->process();
        }

        return $this->render('key-x', [
            'model'  => $model,
        ]);
    }
}
