<?php
return [
    'Marketing'       => 'Маркетинг',
    'KEY Services'    => 'Работа с ключами',
    'Keys proccessed' => 'Обработка ключей',

    'utmParams'         => 'UTM метки',
    'titles'            => 'Заголовки',
    'texts'             => 'Текста',
    'links'             => 'Сссылки',
    'keys'              => 'Ключи',
    'wordDropInLength'  => 'Удаляемые слова при max length',
    'wordReplacesTitle' => 'Заменяемы слова в заголовке',
    'wordReplacesText'  => 'Заменяемы слова в тексте',
    'wordFindLinks'     => 'Ассоциации ссылок по словам',
    'textTemplate'      => 'Шаблон текста',
    'fastlinkTitles'    => 'Быстрые ссылки - заголовки',
    'fastlinkLinks'     => 'Быстрые ссылки - ссылки',

    'Keys X'     => 'Перемножение слов',
];
