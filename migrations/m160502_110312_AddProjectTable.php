<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160502_110312_AddProjectTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('adv_project', [
            'id'          => $this->primaryKey(11),
            'code'        => $this->string(255)->notNull(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->string(14),
            'settings'    => 'jsonb NULL DEFAULT \'{}\'',
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_adv_project_created_by',
            'adv_project',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_adv_project_updated_by',
            'adv_project',
            'updated_by',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('adv_project');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
