<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160502_110315_AddCompanyTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('adv_company', [
            'id'          => $this->primaryKey(11),
            'project_id'  => $this->integer()->notNull(),
            'code'        => $this->string(50)->notNull(),
            'name'        => $this->string(50)->notNull(),
            'description' => $this->text(),

            'settings' => 'jsonb NULL DEFAULT \'{}\'',
            'mindmap'  => 'jsonb NULL DEFAULT \'{}\'',

            'group'      => $this->string(150)->notNull(),
            'platform'   => $this->smallInteger(1)->notNull(),
            'type'       => $this->smallInteger(1)->notNull(),
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

       /* $this->addForeignKey(
            'fk_adv_company_project_id',
            'adv_company',
            'project_id',
            'adv_project',
            'id'
        );

        $this->addForeignKey(
            'fk_adv_company_created_by',
            'adv_company',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_adv_company_updated_by',
            'adv_company',
            'updated_by',
            'users_user',
            'id'
        );*/

        //$this->createIndex('files_model', 'files_file', ['object', 'object_id', 'status']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('adv_company');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
