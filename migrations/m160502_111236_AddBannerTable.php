<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160502_111236_AddBannerTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('adv_banner', [
            'id'         => $this->primaryKey(11),
            'project_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'hash'       => $this->string(32),
            'key'        => $this->string(255),
            'key_views'  => $this->integer(),
            'title'      => $this->string(50),
            'text'       => $this->string(100)->notNull(),
            'text2'      => $this->string(100)->notNull(),
            'url'        => $this->string(255)->notNull(),
            'url_view'   => $this->string(255)->notNull(),

            'price1' => $this->decimal(5, 2),
            'price2' => $this->decimal(5, 2),
            'price3' => $this->decimal(5, 2),
            'price4' => $this->decimal(5, 2),
            'price5' => $this->decimal(5, 2),
            'price6' => $this->decimal(5, 2),

            'cost' => $this->decimal(5, 2),

            'roi'          => $this->decimal(3, 2),
            'ctr'          => $this->decimal(3, 2),
            'productivity' => $this->decimal(2, 2),
            'clicks'       => $this->integer(),
            'views'        => $this->integer(),


            'type' => $this->smallInteger(1)->notNull(),

            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_adv_banner_company_id',
            'adv_banner',
            'company_id',
            'adv_company',
            'id'
        );

        $this->addForeignKey(
            'fk_adv_banner_project_id',
            'adv_banner',
            'project_id',
            'adv_project',
            'id'
        );

        $this->addForeignKey(
            'fk_adv_banner_created_by',
            'adv_banner',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_adv_banner_updated_by',
            'adv_banner',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('idx_adv_banner_roi', 'adv_banner', ['roi']);
        $this->createIndex('idx_adv_banner_ctr', 'adv_banner', ['ctr']);
        $this->createIndex('idx_adv_banner_hash', 'adv_banner', ['hash']);
        $this->createIndex('idx_adv_banner_clicks', 'adv_banner', ['project_id', 'clicks']);
        $this->createIndex('idx_adv_banner_views', 'adv_banner', ['project_id', 'views']);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('adv_banner');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
