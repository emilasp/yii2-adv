<?php
namespace emilasp\adv\models\forms;

use yii;
use yii\base\Model;

/**
 * Class DirectForm
 * @package emilasp\adv\models\forms
 */
class DirectForm extends Model
{
    const MAX_LENGTH_TITLE = 32;

    public $utmParams;
    public $keys;
    public $wordDropInLength;
    public $wordReplacesTitle;
    public $wordReplacesText;
    public $wordFindLinks;
    public $textTemplate;
    public $titles;
    public $texts;
    public $links;
    public $linksViewFind;
    public $linksView;
    public $fastlinkTitles;
    public $fastlinkLinks;

    /**
     * @inheritdoc
     */
    public function rules()

    {
        return [
            [
                [
                    'utmParams',

                    'titles',
                    'texts',
                    'links',
                    'linksView',
                    'linksViewFind',
                    'keys',
                    'wordDropInLength',
                    'wordReplacesTitle',
                    'wordReplacesText',
                    'wordFindLinks',
                    'textTemplate',
                    'fastlinkTitles',
                    'fastlinkLinks',
                ],
                'string',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'utmParams' => Yii::t('adv', 'utmParams'),

            'titles'            => Yii::t('adv', 'titles'),
            'texts'             => Yii::t('adv', 'texts'),
            'links'             => Yii::t('adv', 'links'),
            'linksView'         => Yii::t('adv', 'links view'),
            'linksViewFind'     => Yii::t('adv', 'links view find'),
            'keys'              => Yii::t('adv', 'keys'),
            'wordDropInLength'  => Yii::t('adv', 'wordDropInLength'),
            'wordReplacesTitle' => Yii::t('adv', 'wordReplacesTitle'),
            'wordReplacesText'  => Yii::t('adv', 'wordReplacesText'),
            'wordFindLinks'     => Yii::t('adv', 'wordFindLinks'),
            'textTemplate'      => Yii::t('adv', 'textTemplate'),
            'fastlinkTitles'    => Yii::t('adv', 'fastlinkTitles'),
            'fastlinkLinks'     => Yii::t('adv', 'fastlinkLinks'),
        ];
    }


    public function init()
    {
        parent::init();

        if (!$this->wordDropInLength) {
            $this->wordDropInLength = implode("\r\n", [
                '=>недорого',
                '=>женские',
                '=>купить',
                '=>нью баланс',
                '=>интернет магазине',
                '=>интернет магазин',
            ]);
        }

        if (!$this->wordReplacesTitle) {
            $this->wordReplacesTitle = implode("\r\n", [
                'найк аир форс=>Nike Air Force',
                'найк хуарачи=>Nike Huarache',
                'найк аир макс=>Nike Air max',
                'найк аир=>Nike Air',
                'найк макс=>Nike Max',
                'найк джордан=>Nike Air Jordan',
                'найк шокс=>Nike Shox',
                'найк=>Nike',

                'адидас кундо=>Adidas Kundo',
                'адидас боунс=>Adidas Bounce',
                'адидас кампус=>Adidas Campus',
                'адидас нео=>Adidas Neo',
                'адидас перфоманс=>Adidas Performance',
                'адидас гамбург=>Adidas Hamburg',

                'адидас терекс=>Adidas terrex',
                'адидас спрингблейд=>Adidas Springblade',
                'адидас гудиер=>Adidas Goodyear',
                'адидас суперстар=>Adidas Superstar',
                'адидас порше=>Adidas Porshe',
                'адидас=>Adidas',
                'addidas=>Adidas',


                'нью баланс=>New balance',

                'пума бмв=>Puma BMW',
                'пума феррари=>Puma Ferrari',
                'пума=>Puma',

                'рибок памп=>Reebok Pump',
                'рибок рефлекс=>Reebok Reflex',
                'рибок изитон=>Reebok EasyTone',
                'рибок=>Reebok',

                'асикс нимбус=>Asics Nimbus',
                'асикс=>Asics',


                'аир=>Air',
                'форсе=>Force',
                'форс=>Force',
                'макс=>max',
                'винтер=>winter',
                'роше=>Roshe',
                'фри=>Free',
                'кортез=>Cortez',
                'круз=>Cruz',
                'скайлайн=>Skyline',
                'гиперфусе=>Hyperfuse',
                'хуарачи=>Huarache',
                'джордан=>Jordan',
                'найк=>Nike',
                'терекс=>terrex',
                'спрингблейд=>Springblade',
                'гудиер=>Goodyear',
                'суперстар=>Superstar',
                'порше=>Porshe',
                'адидас=>Adidas',
                'addidas=>Adidas',
                'буст=>Boost',
                'эквипмент=>Equipment',
                'газель=>Gazelle',
                'гудеар=>Goodyear',
                'марафон=>Marathon',
                'флайкнит=>Flyknit',
                'раннер=>Runner',
                'ранер=>Runner',
                'дизайн=>Design',
                'рансом=>Ransom',
                'табулар=>Tubular',
                'ези=>Yeezy',
                'иззи=>Yeezy',
                'еези=>Yeezy',
                'нью=>New',
                'баланс=>Balance',
                'бмв=>BMW',
                'пума=>Puma',
                'феррари=>Ferrari',
                'рибок=>Reebok',
                'памп=>Pump',
                'рефлекс=>Reflex',
                'изитон=>EasyTone',
                'нимбус=>Nimbus',
                'асикс=>Asics',
                'конверс=>Converse',


                'москв=>Москв',
                'зеленоград=>Зеленоград',


                'Кроссовок=>Кроссовки',
                'стоимость=>не дорого',
                'цена=>не дорого',
                'продам=>дешево',
                'кросовки=>кроссовки',
                'доставить=>доставка',
                'покупка=>купить',
            ]);
        }


        if (!$this->wordReplacesText) {
            $this->wordReplacesText = implode("\r\n", [
                '{BRAND}=>New balance',
                '{BRAND}=>Nike',
                '{BRAND}=>Adidas',
                '{BRAND}=>Puma',
                '{BRAND}=>Reebok',
                '{BRAND}=>Asics',
            ]);
        }

        if (!$this->wordFindLinks) {
            $this->wordFindLinks = implode("\r\n", array_reverse([
                'аир макс=>http://sport-run.ru/im/brand/5/nike.html',
                'Nike=>http://sport-run.ru/im/brand/5/nike.html',
                'Adidas=>http://sport-run.ru/im/brand/12/adidas.html',
                'New balance=>http://sport-run.ru/im/brand/4/new-balance.html',
                'Puma=>http://sport-run.ru/im/brand/11/puma.html',
                'Reebok=>http://sport-run.ru/im/brand/8/reebok.html',
                'Asics=>http://sport-run.ru/im/brand/7/asics.html',
                'Nike=>http://sport-run.ru/im/brand/5/nike.html',
                'найк=>http://sport-run.ru/im/brand/5/nike.html',
                'айр=>http://sport-run.ru/im/brand/5/nike.html',
                'air=>http://sport-run.ru/im/brand/5/nike.html',
                'force=>http://sport-run.ru/im/model/22/nike-air-force.html',
                'форс=>http://sport-run.ru/im/model/22/nike-air-force.html',
                'форсе=>http://sport-run.ru/im/model/22/nike-air-force.html',
                'jordan=>http://sport-run.ru/im/model/14/nike-air-jordan.html',
                'джордан=>http://sport-run.ru/im/model/14/nike-air-jordan.html',
                'max 2013=>http://sport-run.ru/im/model/19/nike-air-max-2013.html',
                'air 2013=>http://sport-run.ru/im/model/19/nike-air-max-2013.html',
                'air max 2013=>http://sport-run.ru/im/model/19/nike-air-max-2013.html',
                'макс 2013=>http://sport-run.ru/im/model/19/nike-air-max-2013.html',
                'max 2014=>http://sport-run.ru/im/model/31/nike-air-max-2014.html',
                'air 2014=>http://sport-run.ru/im/model/31/nike-air-max-2014.html',
                'air max 2014=>http://sport-run.ru/im/model/31/nike-air-max-2014.html',
                'макс 2014=>http://sport-run.ru/im/model/31/nike-air-max-2014.html',
                'max 2015=>http://sport-run.ru/im/model/34/nike-air-max-2015.html',
                'air 2015=>http://sport-run.ru/im/model/34/nike-air-max-2015.html',
                'air max 2015=>http://sport-run.ru/im/model/34/nike-air-max-2015.html',
                'макс 2015=>http://sport-run.ru/im/model/34/nike-air-max-2015.html',
                'air 2016=>http://sport-run.ru/im/model/32/nike-air-max-2016.html',
                'max 2016=>http://sport-run.ru/im/model/32/nike-air-max-2016.html',
                'air max 2016=>http://sport-run.ru/im/model/32/nike-air-max-2016.html',
                'макс 2016=>http://sport-run.ru/im/model/32/nike-air-max-2016.html',
                'air 87=>http://sport-run.ru/im/model/27/nike-air-max-87.html',
                'max 87=>http://sport-run.ru/im/model/27/nike-air-max-87.html',
                'air max 87=>http://sport-run.ru/im/model/27/nike-air-max-87.html',
                'макс 87=>http://sport-run.ru/im/model/27/nike-air-max-87.html',
                'air 90=>http://sport-run.ru/im/model/5/nike-air-max-90.html',
                'max 90=>http://sport-run.ru/im/model/5/nike-air-max-90.html',
                'air max 90=>http://sport-run.ru/im/model/5/nike-air-max-90.html',
                'макс 90=>http://sport-run.ru/im/model/5/nike-air-max-90.html',
                'hyperfuse=>http://sport-run.ru/im/model/24/nike-air-max-90-hyperfuse.html',
                'гиперфусе=>http://sport-run.ru/im/model/24/nike-air-max-90-hyperfuse.html',
                'вт=>http://sport-run.ru/im/model/23/nike-air-max-90-vt.html',
                'vt=>http://sport-run.ru/im/model/23/nike-air-max-90-vt.html',
                'air 95=>http://sport-run.ru/im/model/20/nike-air-max-95.html',
                'макс 95=>http://sport-run.ru/im/model/20/nike-air-max-95.html',
                'skyline=>http://sport-run.ru/im/model/35/nike-air-max-skyline.html',
                'скайлайн=>http://sport-run.ru/im/model/35/nike-air-max-skyline.html',
                'cruz=>http://sport-run.ru/im/model/16/nike-air-trainer-cruz.html',
                'trainer=>http://sport-run.ru/im/model/16/nike-air-trainer-cruz.html',
                'круз=>http://sport-run.ru/im/model/16/nike-air-trainer-cruz.html',
                'cortez=>http://sport-run.ru/im/model/28/nike-cortez.html',
                'кортез=>http://sport-run.ru/im/model/28/nike-cortez.html',
                'фри=>http://sport-run.ru/im/model/30/nike-free-run.html',
                'free=>http://sport-run.ru/im/model/30/nike-free-run.html',
                'huarache=>http://sport-run.ru/im/model/10/nike-huarache.html',
                'хуарачи=>http://sport-run.ru/im/model/10/nike-huarache.html',
                'роше=>http://sport-run.ru/im/model/26/nike-roshe-run.html',
                'roshe=>http://sport-run.ru/im/model/26/nike-roshe-run.html',
                'winter=>http://sport-run.ru/im/model/51/nike-winter-zimnie-s-mexom.html',
                'винтер=>http://sport-run.ru/im/model/51/nike-winter-zimnie-s-mexom.html',
                'Puma=>http://sport-run.ru/im/brand/11/puma.html',
                'адидас=>http://sport-run.ru/im/brand/12/adidas.html',
                'Addidas=>http://sport-run.ru/im/brand/12/adidas.html',
                'буст=>http://sport-run.ru/im/model/52/adidas-boost.html',
                'boost=>http://sport-run.ru/im/model/52/adidas-boost.html',
                'equipment=>http://sport-run.ru/im/model/44/adidas-equipment-93.html',
                'эквипмент=>http://sport-run.ru/im/model/44/adidas-equipment-93.html',
                'газель=>http://sport-run.ru/im/model/53/adidas-gazelle.html',
                'gazelle=>http://sport-run.ru/im/model/53/adidas-gazelle.html',
                'goodyear=>http://sport-run.ru/im/model/38/adidas-goodyear.html',
                'гудеар=>http://sport-run.ru/im/model/38/adidas-goodyear.html',
                'marathon=>http://sport-run.ru/im/model/60/adidas-marathon-flyknit.html',
                'flyknit=>http://sport-run.ru/im/model/60/adidas-marathon-flyknit.html',
                'марафон=>http://sport-run.ru/im/model/60/adidas-marathon-flyknit.html',
                'флайкнит=>http://sport-run.ru/im/model/60/adidas-marathon-flyknit.html',
                'раннер=>http://sport-run.ru/im/model/49/adidas-nmd-runner.html',
                'ранер=>http://sport-run.ru/im/model/49/adidas-nmd-runner.html',
                'мнд=>http://sport-run.ru/im/model/49/adidas-nmd-runner.html',
                'mnd=>http://sport-run.ru/im/model/49/adidas-nmd-runner.html',
                'runner=>http://sport-run.ru/im/model/49/adidas-nmd-runner.html',
                'porsche=>http://sport-run.ru/im/model/1/adidas-porsche-design.html',
                'design=>http://sport-run.ru/im/model/1/adidas-porsche-design.html',
                'порше=>http://sport-run.ru/im/model/1/adidas-porsche-design.html',
                'дизайн=>http://sport-run.ru/im/model/1/adidas-porsche-design.html',
                'рансом=>http://sport-run.ru/im/model/54/adidas-ransom.html',
                'ransom=>http://sport-run.ru/im/model/54/adidas-ransom.html',
                'springblade=>http://sport-run.ru/im/model/37/adidas-springblade.html',
                'спрингблейд=>http://sport-run.ru/im/model/37/adidas-springblade.html',
                'суперстар=>http://sport-run.ru/im/model/25/adidas-superstar.html',
                'superstar=>http://sport-run.ru/im/model/25/adidas-superstar.html',
                'terrex	http://sport-run.ru/im/model/59/adidas-terrex.html',
                'террекс=>http://sport-run.ru/im/model/59/adidas-terrex.html',
                'tubular=>http://sport-run.ru/im/model/47/adidas-tubular.html',
                'табулар=>http://sport-run.ru/im/model/47/adidas-tubular.html',
                'winter=>http://sport-run.ru/im/model/57/adidas-winter-zimnie-s-mexom.html',
                'винтер=>http://sport-run.ru/im/model/57/adidas-winter-zimnie-s-mexom.html',
                'yeezy=>http://sport-run.ru/im/model/6/adidas-yeezy-boost.html',
                'boost=>http://sport-run.ru/im/model/6/adidas-yeezy-boost.html',
                'ези=>http://sport-run.ru/im/model/6/adidas-yeezy-boost.html',
                'иззи=>http://sport-run.ru/im/model/6/adidas-yeezy-boost.html',
                'еези=>http://sport-run.ru/im/model/6/adidas-yeezy-boost.html',
                'zx750=>http://sport-run.ru/im/model/15/adidas-zx750.html',
                'зх 750=>http://sport-run.ru/im/model/15/adidas-zx750.html',
                'puma=>http://sport-run.ru/im/brand/11/puma.html',
                'пума=>http://sport-run.ru/im/brand/11/puma.html',
                'рибок=>http://sport-run.ru/im/brand/8/reebok.html',
                'reebok=>http://sport-run.ru/im/brand/8/reebok.html',
                'баланс=>http://sport-run.ru/im/brand/4/new-balance.html',
                'balance=>http://sport-run.ru/im/brand/4/new-balance.html',
                '1400=>http://sport-run.ru/im/model/48/new-balance-1400.html',
                '574=>http://sport-run.ru/im/model/8/new-balance-574.html',
                '576=>http://sport-run.ru/im/model/11/new-balance-576.html',
                '577=>http://sport-run.ru/im/model/39/new-balance-577.html',
                '580=>http://sport-run.ru/im/model/12/new-balance-580.html',
                '670=>http://sport-run.ru/im/model/2/new-balance-670.html',
                '988=>http://sport-run.ru/im/model/41/new-balance-988.html',
                '990=>http://sport-run.ru/im/model/40/new-balance-990.html',
                '996=>http://sport-run.ru/im/model/36/new-balance-996.html',
                '997=>http://sport-run.ru/im/model/45/new-balance-997.html',
                '998=>http://sport-run.ru/im/model/46/new-balance-998.html',
                '999=>http://sport-run.ru/im/model/43/new-balance-999.html',
                'асикс=>http://sport-run.ru/im/brand/7/asics.html',
                'asics=>http://sport-run.ru/im/brand/7/asics.html',
                'конверс=>http://sport-run.ru/im/brand/3/converse.html',
                'Converse=>http://sport-run.ru/im/brand/3/converse.html',
                'allstar=>http://sport-run.ru/im/model/17/converse-all-star.html',
                'олстар=>http://sport-run.ru/im/model/17/converse-all-star.html',
                'default=>http://sport-run.ru/',
            ]));
        }

        if (!$this->textTemplate) {
            $this->textTemplate = 'Распродажа кроссовок {BRAND}! Скидки до 70! Успей купить онлайн!';
        }

        if (!$this->utmParams) {
            $this->utmParams = '?utm_source=yandex&utm_medium=cpc&utm_campaign=sport_run_zel&type={source_type}&source={source}&block={position_type}&position={position}&keyword={keyword}';
        }

        if (!$this->fastlinkTitles) {
            $this->fastlinkTitles = 'Nike||Adidas||New Balance||Reebok||Puma||Asics';
        }
        if (!$this->fastlinkLinks) {
            $this->fastlinkLinks = 'http://sport-run.ru/im/brand/5/nike.html#nike||http://sport-run.ru/im/brand/12/adidas.html#adidas||http://sport-run.ru/im/brand/4/new-balance.html#new_balance||http://sport-run.ru/im/brand/8/reebok.html#reebok||http://sport-run.ru/im/brand/11/puma.html#puma||sport-run.ru/im/brand/7/asics.html#asics';
        }
        if (!$this->linksViewFind) {
            $this->linksViewFind = implode("\r\n", [
                'default=>http://sport-run.ru/интернет_магазин_распродажа',
                'sale=>http://sport-run.ru/интернет_магазин_диконт',
                'не дорого=>http://sport-run.ru/интернет_магазин_диконт',
                'для бега=>http://sport-run.ru/стильная_спортивная_обувь',
                'модные=>http://sport-run.ru/стильная_спортивная_обувь',
                'новые=>http://sport-run.ru/стильная_спортивная_обувь',
                'скидки=>http://sport-run.ru/интернет_магазин_скидки',
                'акции=>http://sport-run.ru/интернет_магазин_акция',
                'купить=>http://sport-run.ru/интернет_магазин_диконт',
                'доставить=>http://sport-run.ru/бесплатная_доставка',
                'приобрести=>http://sport-run.ru/интернет_магазин_диконт',
                'продажа=>http://sport-run.ru/интернет_магазин_диконт',
                'продам=>http://sport-run.ru/интернет_магазин_диконт',
                'покупка=>http://sport-run.ru/интернет_магазин_диконт',
                'цена=>http://sport-run.ru/интернет_магазин_диконт',
                'стоимость=>http://sport-run.ru/интернет_магазин_диконт',
                'доставка=>http://sport-run.ru/бесплатная_доставка',
                'заказать=>http://sport-run.ru/интернет_магазин_диконт',
                'заказ=>http://sport-run.ru/интернет_магазин_диконт',
            ]);
        }
    }

    /**
     * Генерируем тайтлы, текста и ссылки
     */
    public function generate()
    {
        $this->titles = [];
        $this->texts  = [];
        $this->links  = [];
        $this->linksView  = [];

        $this->keys = str_replace('"', '', $this->keys);

        $this->genTitles();

        $this->titles    = implode("\r\n", $this->titles);
        $this->texts     = implode("\r\n", $this->texts);
        $this->links     = implode("\r\n", $this->links);
        $this->linksView = implode("\r\n", $this->linksView);
    }

    /**
     * Генерируем тайтлы
     */
    private function genTitles()
    {
        $titles = $this->textToArr($this->keys);

        $titles     = array_map('trim', $titles);
        $this->keys = '"' . implode("\"\n\"", $titles) . '"';


        foreach ($titles as $index => $title) {
            $title = $this->replace($title, $this->wordReplacesTitle);

            $title = $this->lengthLimit($title, $this->wordDropInLength);

            $enc   = 'UTF-8';
            $title = mb_strtoupper(mb_substr($title, 0, 1, $enc), $enc) .
                     mb_substr($title, 1, mb_strlen($title, $enc), $enc);


            $this->titles[$index] = $title . '!';
            $this->texts[$index]  = $this->genTexts($title);
            $this->links[$index]  = $this->genLinks($title);
            $this->linksView[$index]  = $this->genLinksView($title);
        }
    }


    /** Удаляем лишние словосочетания
     *
     * @param $title
     * @param $rules
     *
     * @return mixed
     */
    private function lengthLimit($title, $rules)
    {
        $rules = $this->getRules($rules);

        foreach ($rules[1] as $index => $find) {
            $length = mb_strlen($title);
            if ($length > self::MAX_LENGTH_TITLE) {
                $title = str_replace(trim($find), '', $title);
                $title = preg_replace("|[\s]+|is", ' ', $title);
            } else {
                break;
            }
        }
        return trim($title);
    }

    /** Генерируем ссылки
     *
     * @param $title
     *
     * @return string
     */
    private function genLinks($title)
    {
        $link     = '';
        $ruleData = $this->getRules($this->wordFindLinks);
        foreach ($ruleData[0] as $index => $find) {
            if (stripos($title, $find) !== false) {
                $link = $ruleData[1][$index] . '' . $this->utmParams;
            }
        }
        if (!$link) {
            $link = $ruleData[1][0] . '' . $this->utmParams;
        }

        return $link;
    }

    /** Генерируем ссылки(виды)
     *
     * @param $title
     *
     * @return string
     */
    private function genLinksView($title)
    {
        $link     = '';
        $ruleData = $this->getRules($this->linksViewFind);
        foreach ($ruleData[0] as $index => $find) {
            if (stripos($title, $find) !== false) {
                $link = $ruleData[1][$index];
            }
        }
        if (!$link) {
            $link = $ruleData[1][0];
        }

        return $link;
    }

    /** Генерируем текста
     *
     * @param $title
     *
     * @return mixed
     */
    private function genTexts($title)
    {
        $text     = $this->textTemplate;
        $ruleData = $this->getRules($this->wordReplacesText);
        foreach ($ruleData[1] as $index => $find) {
            if (stripos($title, $find) !== false) {
                $text = str_replace($ruleData[0][$index], $find, $text);
            }
        }
        $text = str_replace('{BRAND}', '', $text);
        return $text;
    }


    /** Формируем массивы для правил
     *
     * @param $rules
     *
     * @return array
     */
    private function getRules($rules)
    {
        if (is_array($rules)) {
            return $rules;
        }

        $rules        = $this->textToArr($rules);
        $rulesSearch  = [];
        $rulesReplace = [];
        foreach ($rules as $rule) {
            if (strpos($rule, '=>') !== false) {
                $dataRule       = explode('=>', $rule);
                $rulesSearch[]  = $dataRule[0];
                $rulesReplace[] = $dataRule[1];
            }
        }

        return [$rulesSearch, $rulesReplace];
    }

    /** Осуществляем замену по правилам
     *
     * @param $string
     * @param $rules
     *
     * @return mixed
     */
    private function replace($string, $rules)
    {
        $ruleData = $this->getRules($rules);

        return str_ireplace($ruleData[0], $ruleData[1], $string);
    }

    /** Получаем массив из текста(построчно)
     *
     * @param $string
     *
     * @return mixed
     */
    private function textToArr($string)
    {
        return preg_split('/\\r\\n?|\\n/', $string);
    }
}
