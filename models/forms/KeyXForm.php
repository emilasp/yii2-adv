<?php
namespace emilasp\adv\models\forms;

use yii;
use yii\base\Model;

/**
 * Class KeyXForm
 * @package emilasp\adv\models\forms
 */
class KeyXForm extends Model
{
    const MAX_LENGTH_TITLE = 32;

    public $keys;
    public $result;
    public $count;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['count'], 'number',],
            [['result'], 'string',],
            [['keys'], 'safe',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'keys'   => Yii::t('adv', 'Keys'),
            'result' => Yii::t('adv', 'Result'),
        ];
    }

    public function init()
    {
        parent::init();

        $this->keys = ['', '', '', '', '', ''];
        $this->count = 0;
    }


    public function process()
    {
        $this->count = 0;
        $this->result = [];
        $keysArr = [];
        foreach ($this->keys as $keys) {
            $keysArr[] = $this->textToArr($keys);
        }

        $this->addKeysRecursive('', $keysArr);

        $this->result  = implode("\r\n", $this->result);
       /* $this->result = preg_split('/\\r\\n?|\\n/', $keysAll);*/
    }

    private function addKeysRecursive($keyParent, $keysAll)
    {
        $returnKeys = [];

        if (count($keysAll) > 1) {
            $arrKeysToReq = $keysAll;
            unset($arrKeysToReq[0]);
            $arrKeysToReq = array_merge([], $arrKeysToReq);

            foreach ($keysAll[0] as $key) {
                $returnKeys = $this->addKeysRecursive($keyParent . ' ' . $key, $arrKeysToReq);
            }
        } else {
            foreach ($keysAll[0] as $key) {
                $this->result[] = $keyParent . ' ' . $key;
                $this->count++;
            }
        }
        return $returnKeys;
    }

    /** Получаем массив из текста(построчно)
     *
     * @param $string
     *
     * @return mixed
     */
    private function textToArr($string)
    {
        return preg_split('/\\r\\n?|\\n/', $string);
    }
}
