<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\seo\common\models\Seo */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('seo', 'Seos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('seo', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>


    <?= $form->field($model, 'utmParams')->textInput() ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'fastlinkTitles')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fastlinkLinks')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'keys')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'wordFindLinks')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'linksViewFind')->textarea(['rows' => 5]) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'textTemplate')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'wordDropInLength')->textarea(['rows' => 5]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'wordReplacesTitle')->textarea(['rows' => 5]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'wordReplacesText')->textarea(['rows' => 5]) ?>
        </div>
    </div>
    <hr />
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'titles')->textarea(['rows' => 7]) ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'texts')->textarea(['rows' => 7]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'linksView')->textarea(['rows' => 7]) ?>
        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'links')->textarea(['rows' => 7]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            Yii::t('site', 'Run'),
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
