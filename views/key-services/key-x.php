<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\seo\common\models\Seo */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('seo', 'Seos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('seo', 'Seos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seo-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->errorSummary($model); ?>

    <?php foreach ($model->keys as $index => $key) : ?>
        <?= $form->field($model, 'keys[' . $index . ']')->textarea(['rows' => 5]) ?>
    <?php endforeach; ?>


    <?= $form->field($model, 'result')->textarea(['rows' => 5]) ?>
    <?= $form->field($model, 'count')->textInput() ?>


    <div class="form-group">
        <?= Html::submitButton(
            Yii::t('site', 'Run'),
            ['class' => 'btn btn-success']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
